%% Practicas de Matlab
%% Resolución de EDO con métodos monopaso
%% Hoja 2
% *Nombre: Pablo Cayo*
% 
% *Apellido: Alcalde Montes de Oca*
% 
%% 
% 
%% 1. Implementación de métodos explícitos
% Práctica 1 (Implementación del método de Euler explícito) Escribir en el Apéndice 
% A1 una función implementando el método de Euler (explícito) 
% 
% $$      \left\{\begin{array}{l}               y_{i+1}=y_i + h f(t_i,y_i) \quad 
% i=0,\ldots ,N-1          \\               y_0 \approx a        \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieuler(f,intv,y0,N)|
% 
% El pseudocódigo correspondiente se encuentra en el CV (campus virtual). Práctica 
% 2 (Implementación del método de Euler modificado explícito) Escribir en el Apéndice 
% A1 una función que implemente el método de Euler modificado (explícito) 
% 
% $$\begin{array}{ccl}  y_{i+1} &=& y_i + h f\left(t_i + \frac{h}{2}, y_i + 
% \frac{h}{2} f(t_i,y_i)\right), \quad  i=0,\ldots ,N-1 \\  y_0 &\approx& a\end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieulermod(f,intv,y0,N)| Práctica 3 (Implementación del método de Euler 
% mejorado explícito) Escribir en el Apéndice A1 una función que implemente el 
% método de Euler mejorado (explícito) 
% 
% $$\begin{array}{ccl}y_{i+1} &=& y_i +  \left.{h\over 2} (f(t_i,y_i) + f(t_{i+1},  
% y_i+hf(t_i,y_i)\right), \quad i=0,\ldots ,N-1\\ y_0 &\approx& a\end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieulermej(f,intv,y0,N)| Práctica 4 (Implementación del método de Runge-Kutta 
% explícito) Escribir en el Apéndice A1 una función que implemente el método de 
% Euler mejorado (explícito) 
% 
% $$    \begin{array}{ccl}      y_{i+1} &=& y_i + h \Phi(t_i,y_i,h), \quad i=0,\ldots 
% ,N-1 \\      y_0 &\approx& a    \end{array}$$
% 
% donde $\Phi(t,y,h)=\frac{1}{6}\left(F_1+2F_2+2F_3+F_4\right)$ y 
% 
% $$    \begin{array}{l}      F_1=f(t,y)\\      F_2=f\left(t+\frac{h}{2},y+\frac{h}{2}F_1\right)\\      
% F_3=f\left(t+\frac{h}{2},y+\frac{h}{2}F_2\right) \\      F_4=f\left(t+h,y+hF_3\right),    
% \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mirk4(f,intv,y0,N)|
% 
% % %% Práctica 1 (EDO de corazón) Considera el siguiente PVI
% 
% $$    \begin{array}{ccc}    \frac{dx_1}{dt} & = & x_2                 \\    
% \frac{dx_2}{dt} & = & -16x_1 + 4 \sin(2t) \\    x_1(0)          & = & 0                  
% \\    x_2(0)          & = & 2    \end{array}$$
% 
% en el intervalo, $[0,2 \pi]$ con $N=200$. Ahora intenta resolverla numéricamente 
% usando
%% 
% # el método de Euler 
% # el método de Euler modificado
% # el método de Euler mejorado 
% # el método de Runge Kutta 4 
%% 
% pinta las componentes de la solución y el diagrama de fases.
% 
% *Solución*
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
%% Datos del Problema
disp('Eso es el codigo de Pab')
f = @(t, y) [y(2); -16*y(1)+4*sin(2*t)]; % Defining the funcion for future reference
intv=[0 2*pi];
x0=[0;2];
N = 2000;
%% Resolucion por el Metodo de Euler
met='euler';met='euler';
s=sprintf('Ecuacion de Corazon;\n met=%s;intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x]=mieuler(f,intv,x0,N);
phaseplot(t,x,s)
%% Resolucion por el Metodo de Euler Modificado
met='eulermod';
s=sprintf('Ecuacion de Corazon;\n met=%s;intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x]=mieulermod(f,intv,x0,N);
phaseplot(t,x,s)
%% Resolucion por el Metodo de Euler Mejorado
met='eulermej';
s=sprintf('Ecuacion de Corazon;\n met=%s;intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x]=mieulermej(f,intv,x0,N);
phaseplot(t,x,s)
%% Resolucion por el Metodo de Runge-Kutta
met='runge-kutta4';
s=sprintf('Ecuacion de Corazon;\n met=%s;intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x]=mirk4(f,intv,x0,N);
phaseplot(t,x,s)
%% Apéndice código: funciones de Euler, Euler modificado, Euler mejorado y Runge-Kutta 4
function phaseplot(t,x,s)
sizeofx = size(x);
dimofx = sizeofx(1);
disp("Calling Pab's Plotting Function for")
disp(s)
if dimofx >= 3
    return
end
cc=['r-','r','r','g'];
nc=['t',"x_1(t)","x_2(t)","x_3(t)"];
figure('Name',"Componentes de la Soluci\'on",'NumberTitle','off');
if nargin == 3
end
for n = 1:dimofx
    subplot(dimofx,1,n)
    plot(t,x(n,:),cc(n))
    xlabel(nc(1))
    ylabel(nc(n+1))
end
figure('Name',"Trayectoria de la Soluci\'on",'NumberTitle','off')
if dimofx == 2
    plot(x(1,:),x(2,:),cc(end))
    title(s)
    xlabel(nc(2));
    ylabel(nc(3));
elseif dimofx == 3
    plot3(x(1,:),x(2,:),x(3,:),cc(end))
    xlabel(nc(2));
    ylabel(nc(3));
    zlabel(nc(4));
end
end
function [t,y] = mieuler(f,intv,y0,N)
% Numerical integration using the Explicit Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    slope = f(ti,y_ti);
    ti = ti + h;
    y_ti = y_ti + h*slope;
    t = [t, ti];
    y = [y, y_ti];
end
end
function [t,y] = mieulermej(f,intv,y0,N)
% Numerical integration using the Improved Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    slope = f(ti,y_ti);
    slope2 = f(ti+h, y_ti+h*slope);
    ti = ti + h;
    y_ti = y_ti + h*(slope + slope2)/2;
    t = [t, ti];
    y = [y, y_ti];
end
end
function [t,y] = mieulermod(f,intv,y0,N)
% Numerical integration using the Modified Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    slope = f(ti,y_ti);
    slope = f(ti+h/2, y_ti+h/2*slope);
    ti = ti + h;
    y_ti = y_ti + h*slope;
    t = [t, ti];
    y = [y, y_ti];
end
end
function [t,y] = mirk4(f,intv,y0,N)
% Numerical integration using the Runge-Kutta Order 4 Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    F1 = f(ti,y_ti);
    F2 = f(ti+h/2, y_ti+h/2*F1);
    F3 = f(ti+h/2, y_ti+h/2*F2);
    F4 = f(ti+h, y_ti+h*F3);
    phi = 1/6*(F1+F2*2+F3*2+F4);
    ti = ti + h;
    y_ti = y_ti + h*phi;
    t = [t, ti];
    y = [y, y_ti];
end
end