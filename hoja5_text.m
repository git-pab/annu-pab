%% Prácticas de Matlab
%% Métodos multi pasos
%% Hoja 5
% *Nombre: Pablo C.*
% 
% *Apellido: Alcalde *
% 
% **
%% Práctica 1 Ecuaciones en diferencias
% Escribir en un fichero instrucciones de MATLAB que, tomando como datos $x_{0}$,  
% $x_{1}$ y $N$, calculen los términos de la sucesión
% 
% $$    \begin{cases}      x_{n+2} & = {7 \over 3} x_{n+1} - {2 \over 3} 
% x_{n} \\      & x_{0}, \ x_{1} \quad \mbox{dados}    \end{cases}$$
% 
% para $n=0, \ldots, N$. Los resultados han de almacenarse en la tabla $x$. 
% Además haz una gráfica de $x_{n}$ contra $n$.
% 
% *Solución:*

clear all
close all
x0=0;
x1=1;
N=100;
x=[x0;x1];
for i=1:N-1
    x=[x ;7/3*x(end)-2/3*x(end-1)];
end
disp(x)
plot(0:N,x)
hold on
s=sprintf("Grafica de la sucesion con x0=%g x1=%g con N=%g",x0,x1,N);
title(s);
xlabel("n")
ylabel("X_n")
hold off
%% Práctica 2 Leap frog (Ecuación escalar)
% Consideramos el método de Leap-frog.
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerar la EDO
% 
% $$\begin{cases}y^{\prime}&=y\\y(0)&=1\end{cases}$$
close all
clear all
f = @(t,y) y;
f_exact = @(t,y) exp(t);
x0=1;
N0=200;
intv=[0,1];
[t,y] = mileapfrogalt(f,intv,x0,N0);
plot(t,y)
%% El metodo funciona
N=[];
for j=1:7        
    N(j)=N0*2^j;
end
er_mileapfrog=[];
for n=N
    er_mileapfrog = [er_mileapfrog error_global(@mileapfrog,f,f_exact,intv,x0,n)];
end
loglog(N,er_mileapfrog,"d-",'DisplayName', 'LeapFrog')
grid on
legend show
xlabel("N")
ylabel("Error")
hold off
slope = (log(er_mileapfrog(7))-log(er_mileapfrog(1)))/(log(N(7))-log(N(1)))
disp('codigo de Pab')
%% Práctica 3 Leap frog (Sistemas de ecuaciones)
% Consideramos el método de Leap-frog.
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$    A=  \left(      \begin{array}{cc}        -2 & 1\\        1 & -2      
% \end{array}    \right)    \qquad      B(t) =          \left(            \begin{array}{cc}              
% 2\sin(t)\\              2(\cos(t)-\sin(t)            \end{array}          \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% * Haz un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la hoja anterior 
% 
% *Solución*

close all
clear all
disp('H5: codigo de Pab')
intv = [0 10];
x0 = [2; 3];
N0 = 200;
dimofy = length(x0);
syms 'ysym' [1 dimofy]
syms tsym
fsym = [-2*ysym(1) + ysym(2) + 2*sin(tsym) ; ysym(1) - 2*ysym(2) + 2*(cos(tsym)-sin(tsym))];
Jfsym = jacobian(fsym,ysym);
f = matlabFunction(fsym, 'vars',{tsym,[ysym.']})
Jf = matlabFunction(Jfsym, 'vars',{tsym,[ysym.']})
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
[t,y] = mileapfrog(f,intv,x0,N0);
phaseplot(t,y,'plot')
N=[];
for j=1:7        
    N(j)=N0*2^j;
end
er_mileapfrog=[];
for n=N
    er_mileapfrog = [er_mileapfrog error_global(@mileapfrogalt,f,f_exact,intv,x0,n)];
end
figure('Name',"Error vs N",'NumberTitle','off');
loglog(N,er_mileapfrog,"d-",'DisplayName', 'LeapFrog')
grid on
legend show
xlabel("N")
ylabel("Error")
hold off
slope = (log(er_mileapfrog(7))-log(er_mileapfrog(1)))/(log(N(7))-log(N(1)))
disp('codigo de Pab')
%% 
% 
% 
% * ademas  $N=1000$ dibuja el error frente la variable $t$.
% 
% *Solucion:*

close all
clear all
disp('H5: codigo de Pab')
intv = [0 10];
x0 = [2; 3];
N0 = 1000;
dimofy = length(x0);
syms 'ysym' [1 dimofy]
syms tsym
fsym = [-2*ysym(1) + ysym(2) + 2*sin(tsym) ; ysym(1) - 2*ysym(2) + 2*(cos(tsym)-sin(tsym))];
Jfsym = jacobian(fsym,ysym);
f = matlabFunction(fsym, 'vars',{tsym,[ysym.']})
Jf = matlabFunction(Jfsym, 'vars',{tsym,[ysym.']})
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
[t,y] = mileapfrog(f,intv,x0,N0);
y_exact=[];
for i = t
    y_exact = [y_exact f_exact(i)];
end
er_y = max(abs(y_exact - y));
plot(t,log(er_y),'DisplayName', 'LeapFrog') 
hold on
grid on
legend show
xlabel("t")
ylabel("log(error_local)")
hold off
%% Práctica 3 BDF
% Implementa el método del *BDF*
% 
% $$  y_{n+2}-\frac{4}{3} y_{n+1}+\frac{1}{3} y_n =\frac{2}{3} h f_{n+2}\,.$$
% 
% *Observacón:*
% 
% * Inicializa el método con un método implícito del mismo orden.
% * En cada paso tienes que resolver una ecuación implícita  $z=g(h,x,z)$. Usa 
% la idea de iteración tipo Newton.
% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haz un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la practica anterior
% 
% *Solucón*

close all 
clear all
disp('H5: codigo de Pab')
intv=[0 10];
x0=[2;3];
dimofy = length(x0);
N0 = 200;
nmax = 10;
TOL = .0001;
syms 'ysym' [1 dimofy]
syms tsym
A = [-2,1;998 -999];
B = [2*sin(tsym) ; 999*(cos(tsym)-sin(tsym))];
fsym = A*(ysym.') + B;
Jfsym = jacobian(fsym,ysym);
f = matlabFunction(fsym, 'vars',{tsym,[ysym.']})
Jf = matlabFunction(Jfsym, 'vars',{tsym,[ysym.']})
s=sprintf("y'= [2y_1 + y_2 + 2sint; y_1 - 2y_2 +2(cost-sint)];\n intv=[%g %g];\n x0=[%g %g];",intv,x0);
[t,x,ev]=miBDF(f,Jf,intv,x0,N0,TOL,nmax);
met = 'BDF2';
disp(sprintf("El n\'umero de evaluaciones con el metodo %s es %g",met, ev))
phaseplot(t,x,s)
%%
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
N=[];
for j=1:7        
    N(j)=N0*2^j;
end
er_miBDF=[];
for n=N
    er_miBDF = [er_miBDF error_global_imp(@miBDF,f,Jf,f_exact,intv,x0,n,TOL,nmax)];
end
figure('Name',"Error vs N",'NumberTitle','off');
loglog(N,er_mileapfrog,"d-",'DisplayName', 'BDF2')
grid on
legend show
xlabel("N")
ylabel("Error")
hold off
slope = (log(er_miBDF(7))-log(er_miBDF(1)))/(log(N(7))-log(N(1)))
disp('codigo de Pab')
%% Apéndice: Las funciones Leap_frog y BDF2
%%
function phaseplot(t,x,s)
sizeofx = size(x);
dimofx = sizeofx(1);
disp("Calling Pab's Plotting Function for")
disp(s)
if dimofx >= 3
    return
end
cc=['r-','r','r','g'];
nc=['t',"x_1(t)","x_2(t)","x_3(t)"];
figure('Name',"Componentes de la Soluci\'on",'NumberTitle','off');
if nargin == 3
end
for n = 1:dimofx
    subplot(dimofx,1,n)
    plot(t,x(n,:),cc(n))
    xlabel(nc(1));
    ylabel(nc(n+1));
end
figure('Name',"Trayectoria de la Soluci\'on",'NumberTitle','off')
if dimofx == 2
    plot(x(1,:),x(2,:),cc(end))
    title(s)
    xlabel(nc(2));
    ylabel(nc(3));
elseif dimofx == 3
    plot3(x(1,:),x(2,:),x(3,:),cc(end))
    xlabel(nc(2));
    ylabel(nc(3));
    zlabel(nc(4));
end
end
function [err] = error_global(met,f,f_exact,intv,x0,N)
% Global error related to the numerical integration given 
% met = @ handle for the numerical method
% f = ultivariate function for which we seek a numerical integration
% f_exact = theorethical value for the integration
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
y_exact = [];
[t,y_met] = met(f,intv,x0,N);
for i = t
    y_exact = [y_exact f_exact(i)];
end
er_M = abs(y_exact - y_met);
err= max(er_M(:));
end
function [err] = error_global_imp(met,f,jf,f_exact,intv,x0,N,TOL,nmax)
% Global error related to the numerical integration given 
% met = @ handle for the numerical method
% f = ultivariate function for which we seek a numerical integration
% f_exact = theorethical value for the integration
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
y_exact = [];
[t,y_euler, eval] = met(f,jf,intv,x0,N,TOL,nmax);
for i = t
    y_exact = [y_exact f_exact(i)];
end
er_M = abs(y_exact - y_euler);
err= max(er_M(:));
end
function [t,y,ev] = miBDF(f,jfunc,intv,y0,N,TOL,nmax)
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
%we need to calculate y1
f_ti = f(ti,y_ti);
ev = ev + 1;
ti = ti + h;
g = @(x) x - (y_ti + h/2*(f_ti+f(ti,x)));
jg = eye(2) - h/2*jfunc(ti,y_ti);
[y_ti, ev_ti] = newtonRaphsonIter(g,jg,y_ti,TOL,nmax);
t = [t, ti];
y = [y, y_ti];
ev = ev + ev_ti;
for i = 1:N-1
    ti = ti + h;
    g = @(x) x - 4/3*y(:,end) + 1/3*y(:,end-1) - 2/3*h*f(ti,x);
    jg = eye(2) - 2/3*h*jfunc(ti,y_ti);
    [y_ti, ev_ti] = newtonRaphsonIter(g,jg,y_ti,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [x, counter] = newtonRaphsonIter(f,jf,x0,TOL,nmax)
% Newton Raphson method for finding roots of a function
% f = function whose roots are to be found
% jh = jacobian matrix for the function 
% x0 = starting point for the iteration 
% TOL = error tolerated
% nmax = maximum number of iterations before error message
x = x0;
counter = 0;
while ( max( abs( f(x) ) )>= TOL )
    counter = counter+1;
    x = x - jf\f(x);
    if (counter == nmax)
        disp(f(x))
        error('Check TOL, fixed point iteration does not converge')
    end
end
end
function [t,y] = mileapfrog(f,intv,y0,N)
% Numerical integration using the Explicit Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
slope = f(ti,y_ti);
slope2 = f(ti+h, y_ti+h*slope);
ti = ti + h;
y_ti = y_ti + h*(slope + slope2)/2;
y = [ y, y_ti ];
t = [ t, ti ];
for i = 1:N-1
    y = [y, y(:,end-1) + 2*h*f(t(end),y(:,end))];
    t = [t, t(end)+h];
end
end
function [t,y] = mileapfrogalt(f,intv,y0,N)
% Numerical integration using the Explicit Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
ntps = 1;
order = 2;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
[y_npts,t_ntps] = intnstepsexp(f,y0,t0,h,ntps,order);
y = [y, y_npts];
t = [t, t_ntps];
for i = 1:N-ntps
    y = [y, y(:,end-1) + 2*h*f(t(end),y(:,end))];
    t = [t, t(end)+h];
end
end
function [y_npts, t_npts] = intnstepsexp(f,y0,t0,h,N,order)
y_npts = []; 
t_npts = [];
y_ti = y0;
ti = t0;
for n=1:N
    switch order
        case 1
            slope = f(ti,y_ti);
            ti = ti + h;
            y_ti = y_ti + h*slope;
            y_npts = [y_npts, y_ti];
            t_npts = [t_npts, ti];
        case 2
            slope = f(ti,y_ti);
            slope2 = f(ti+h, y_ti+h*slope);
            ti = ti + h;
            y_ti = y_ti + h*(slope + slope2)/2;
            y_npts = [y_npts, y_ti];
            t_npts = [t_npts, ti];
        case 4
            F1 = f(ti,y_ti);
            F2 = f(ti+h/2, y_ti+h/2*F1);
            F3 = f(ti+h/2, y_ti+h/2*F2);
            F4 = f(ti+h, y_ti+h*F3);
            phi = 1/6*(F1+F2*2+F3*2+F4);
            ti = ti + h;
            y_ti = y_ti + h*phi;
            y_npts = [y_npts, y_ti];
            t_npts = [t_npts, ti];
        otherwise
            error('El orden del metodo no esta implementado aun')
    end
end
end
function [y_npts, t_npts] = intnstepsimp(f,y0,t0,h,N,order,nmax,TOL)
%no esta implementada aun
y_npts = []; 
t_npts = [];
y_ti = y0;
ti = t0;
for n=1:N
    switch order
        case 1
            slope = f(ti,y_ti);
            ti = ti + h;
            y_ti = y_ti + h*slope;
            y_npts = [y_npts, y_ti];
            t_npts = [t_npts, ti];
        case 2
            slope = f(ti,y_ti);
            slope2 = f(ti+h, y_ti+h*slope);
            ti = ti + h;
            y_ti = y_ti + h*(slope + slope2)/2;
            y_npts = [y_npts, y_ti];
            t_npts = [t_npts, ti];
        case 4
            F1 = f(ti,y_ti);
            F2 = f(ti+h/2, y_ti+h/2*F1);
            F3 = f(ti+h/2, y_ti+h/2*F2);
            F4 = f(ti+h, y_ti+h*F3);
            phi = 1/6*(F1+F2*2+F3*2+F4);
            ti = ti + h;
            y_ti = y_ti + h*phi;
            y_npts = [y_npts, y_ti];
            t_npts = [t_npts, ti];
        otherwise
            error('El orden del metodo no esta implementado aun')
    end
end




end