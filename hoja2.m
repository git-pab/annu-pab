%% 
f = @(t, y) [y(2); -16*y(1)+4*sin(2*t)]; % Defining the funcion for future reference
met='euler';
intv=[0 2*pi];
x0=[0;2];
N = 800;
[t,x]=mieuler(f,intv,x0,N);
phaseplot(t,x)

%% 
function phaseplot(t,x)
    sizeofx = size(x);
    dimofx = sizeofx(1);
    disp('dont mind me, shits working 0')
    if dimofx >= 3
        return
    end
    cc=['r-','r','r','g'];
    nc=['t',"x_1(t)","x_2(t)","x_3(t)"];
    figure('Name',"Componentes de la Soluci\'on",'NumberTitle','off');
    for n = 1:dimofx
        subplot(dimofx,1,n)
        plot(t,x(n,:),cc(n))
        disp('dont mind me, shits working 1')
        xlabel(nc(1))
        ylabel(nc(n+1))
    end
    figure('Name',"Trayectoria de la Soluci\'on",'NumberTitle','off')
    if dimofx == 2
        plot(x(1,:),x(2,:),cc(end))
        xlabel(nc(2));
        ylabel(nc(3));
    elseif dimofx == 3
        plot3(x(1,:),x(2,:),x(3,:),cc(end))
        xlabel(nc(2));
        ylabel(nc(3));
        zlabel(nc(4));
    end
end



function [t,y] = mieuler(f,intv,y0,N)
% Numerical integration using the Explicit Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    slope = f(ti,y_ti);
    ti = ti + h;
    y_ti = y_ti + h*slope;
    t = [t, ti];
    y = [y, y_ti];
end
end