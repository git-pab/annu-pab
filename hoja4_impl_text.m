%% Prácticas de Matlab
%% Resolución de EDO con métodos implícitos
%% Hoja 4
% *Nombre:* Pablo 
% 
% *Apellido:* Alcalde 
% 
%% 1. Implementación de métodos implícitos
%% Práctica 1 (Implementación del método de Euler implícito)
% Escribid en el apéndice A1 4 funciones que implementen el método de Euler 
% (implícito) 
% 
% $$      \left\{\begin{array}{l}               y_{i+1}=y_i + h f(t_{i+1},y_{i+1}) 
% \quad i=0,\ldots ,N-1          \\               y_0 \approx a        \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) en varias maneras 
% Existen las siguientes maneras de implementar dicho método, dependiendo de 
%% 
% * qué tipo de iteración usas para resolver la relación implícita 
% * cómo eliges el dato inicial para esa iteración.
%% 
% En cualquier caso, tenéis que implementar la iteración usando un
%% 
% * *while* y una tolerancia dada.
% * un numero máximo de iteraciones para evitar un bucle infinito.
%% 
% La iteración puede ser bien esto, bien esto
%% 
% * Una iteración simple.
% * Una iteración tipo Newton
%% 
% La elección del punto inicial para la iteración puede ser:
%% 
% * el valor del paso anterior. 
% * el valor calculado por un método explícito, como por ejemplo Euler explícito. 
% (Conocido como  *predictor-corrector*)
%% 
% Por lo tanto, son posibles las siguientes implementaciones:
%%
% * mieulerimpfix: Iteración simple+dato inicial el valor del paso anterior, 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfix(f,intv,y0,N,TOL,nmax)
%
%%
% * mieulernwt: Iteración tipo Newton+dato inicial el valor del paso anterior 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpnwt(f,jf,intv,y0,N,TOL,nmax)
%
%% 
% * meulerfixpc: Iteración simple+dato inicial por el método de Euler y que 
% responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax)
%
%% 
% * mieulernwtpc: Iteración tipo Newton+dato inicial por el método de Euler 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfixpc(f,jf,intv,y0,N,TOL,nmax)
%
%% Solucion Practica 1
disp("Las funciones estan implementadas")
%% Práctica 2 (El método del trapecio)
% Repetid el ejercicio anterior implementando el método del trapecio
%% Solucion Practica 2
disp("Las funciones estan implementadas")

%% Datos del Problema
disp('Eso es el codigo de Pab')
A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) 2*exp(-t)*[1; 1] + [sin(t); cos(t)];
J = A;% Defining the funcion for future reference
intv=[0 10];
x0=[2;3];
N = 100;
nmax = 10;
TOL = .001;
%% Resolucion por el Metodo de Euler Implicito  con Iter de Punto Fijo
met='euler implicito punto fijo';
s=sprintf('Ecuacion a resolver;\n met=%s;\n intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x,ev]=mieulerimpfix(f,intv,x0,N,TOL,nmax);
disp(sprintf("El n\'umero de evaluaciones con el metodo %s es %g",met, ev))
phaseplot(t,x,s)

%% Resolucion por el Metodo de Euler Implicito  con Iter de Punto Fijo (PC)
met='euler implicito punto fijo (pc)';
s=sprintf('Ecuacion a resolver;\n met=%s;\n intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x,ev]=mieulerimpfixpc(f,intv,x0,N,TOL,nmax);
disp(sprintf("El n\'umero de evaluaciones con el metodo %s es %g",met, ev))
phaseplot(t,x,s)

%% Resolucion por el Metodo de Euler Implicito con Newton-Raphson
met='euler implicito newton-raphson';
s=sprintf('Ecuacion a resolver;\n met=%s;\n intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x,ev]=mieulerimpnwt(f,J,intv,x0,N,TOL,nmax);
disp(sprintf("El n\'umero de evaluaciones con el metodo %s es %g",met, ev))
phaseplot(t,x,s)
%% Resolucion por el Metodo de Euler Implicito con Newton-Raphson (PC)
met='euler implicito newton-raphson (pc)';
s=sprintf('Ecuacion a resolver;\n met=%s;\n intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x,ev]=mieulerimpnwtpc(f,J,intv,x0,N,TOL,nmax);
disp(sprintf("El n\'umero de evaluaciones con el metodo %s es %g",met, ev))
phaseplot(t,x,s)
%% Práctica 3 (Ecuación no rígida con Euler implícito)
% Considerad el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$    A=  \left(      \begin{array}{cc}        -2 & 1\\        1 & -2      
% \end{array}    \right)    \qquad      B(t) =          \left(            \begin{array}{cc}              
% 2\sin(t)\\              2(\cos(t)-\sin(t)            \end{array}          \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haced un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la práctica anterior
%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfix* 
%% 
% *Solución*
disp('H4: codigo de Pab')
A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) 2*exp(-t)*[1; 1] + [sin(t); cos(t)];
J = A;% Defining the funcion for future reference
intv=[0 10];
x0=[2;3];
N0 = 100;
nmax = 10;
TOL = .001;
s=sprintf("y'= [2y_1 + y_2 + 2sint; y_1 - 2y_2 +2(cost-sint)];\n intv=[%g %g];\n x0=[%g %g];",intv,x0);
N=[];
for j=1:6        
    N(j)=N0*2^j;
end
er_mieulerimpnwt=[];
for n=N
    er_mieulerimpnwt = [er_mieulerimpnwt error_global_impnwt(@mieulerimpnwt,f,J,f_exact,intv,x0,n,TOL,nmax)];
end
er_mieulerimpfix=[];
for n=N
    er_mieulerimpfix = [er_mieulerimpfix error_global_imp(@mieulerimpfix,f,f_exact,intv,x0,n,TOL,nmax)];
end
loglog(N,er_mieulerimpnwt,"o-",'DisplayName', 'Euler Imp Nwt')
hold on
loglog(N,er_mieulerimpfix,"d-",'DisplayName', 'Euler Imp Fix')
grid on
title(s)
legend show
xlabel("N")
ylabel("Error")
hold off
%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfixpc*
%% 
% *Solución*
disp('H4: codigo de Pab')
A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) 2*exp(-t)*[1; 1] + [sin(t); cos(t)];
J = A;% Defining the funcion for future reference
intv=[0 10];
x0=[2;3];
N0 = 100;
nmax = 10;
TOL = .001;
s=sprintf("y'= [2y_1 + y_2 + 2sint; y_1 - 2y_2 +2(cost-sint)];\n intv=[%g %g];\n x0=[%g %g];",intv,x0);
N=[];
for j=1:6        
    N(j)=N0*2^j;
end
er_mieulerimpnwt=[];
for n=N
    er_mieulerimpnwt = [er_mieulerimpnwt error_global_impnwt(@mieulerimpnwt,f,J,f_exact,intv,x0,n,TOL,nmax)];
end
er_mieulerimpfix=[];
for n=N
    er_mieulerimpfix = [er_mieulerimpfix error_global_imp(@mieulerimpfixpc,f,f_exact,intv,x0,n,TOL,nmax)];
end
loglog(N,er_mieulerimpnwt,"o-",'DisplayName', 'Euler Imp Nwt')
hold on
loglog(N,er_mieulerimpfix,"d-",'DisplayName', 'Euler Imp Fix PC')
grid on
title(s)
legend show
xlabel("N")
ylabel("Error")
hold off
%% 
% * comparando los métodos de *mieulerimpnwtpc*, *mieulerimpfixpc*
%% 
% *Solución*

disp('H4: codigo de Pab')
A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) 2*exp(-t)*[1; 1] + [sin(t); cos(t)];
J = A;% Defining the funcion for future reference
intv=[0 10];
x0=[2;3];
N0 = 100;
nmax = 10;
TOL = .001;
s=sprintf("y'= [2y_1 + y_2 + 2sint; y_1 - 2y_2 +2(cost-sint)];\n intv=[%g %g];\n x0=[%g %g];",intv,x0);
N=[];
for j=1:6        
    N(j)=N0*2^j;
end
er_mieulerimpnwt=[];
for n=N
    er_mieulerimpnwt = [er_mieulerimpnwt error_global_impnwt(@mieulerimpnwtpc,f,J,f_exact,intv,x0,n,TOL,nmax)];
end
er_mieulerimpfix=[];
for n=N
    er_mieulerimpfix = [er_mieulerimpfix error_global_imp(@mieulerimpfixpc,f,f_exact,intv,x0,n,TOL,nmax)];
end
loglog(N,er_mieulerimpnwt,"o-",'DisplayName', 'Euler Imp Nwt PC')
hold on
loglog(N,er_mieulerimpfix,"d-",'DisplayName', 'Euler Imp Fix PC')
grid on
title(s)
legend show
xlabel("N")
ylabel("Error")
hold off
%% 
% * calcula la pendiente de las rectas 

%% 
% *Solución*

disp('H4: codigo de Pab')
disp('algo similar se hizo en la hoja 3, se puede extrapolar')
%% Práctica 4 (Ecuación no rígida con el trapecio)
% Repetid la práctica 3 pero con el método del trapecio de la práctica 2.
%% Resolucion por el Metodo del Trapecio con Newton-Raphson (PC)
met='trapecio newton-raphson (pc)';
s=sprintf('Ecuacion a resolver;\n met=%s;\n intv=[%g %g];\n x0=[%g %g]; N=%g;',met,intv,x0,N);
[t,x,ev]=mitrapnwtpc(f,J,intv,x0,N,TOL,nmax);
fprintf("El n\'umero de evaluaciones con el metodo %s es %g \n",met, ev)
phaseplot(t,x,s)
%% 
% *Solución*
disp('H4: codigo de Pab')
A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) 2*exp(-t)*[1; 1] + [sin(t); cos(t)];
J = A;% Defining the funcion for future reference
intv=[0 10];
x0=[2;3];
N0 = 100;
nmax = 10;
TOL = .001;
s=sprintf("y'= [2y_1 + y_2 + 2sint; y_1 - 2y_2 +2(cost-sint)];\n intv=[%g %g];\n x0=[%g %g];",intv,x0);
N=[];
for j=1:6        
    N(j)=N0*2^j;
end
er_mieulerimpnwt=[];
for n=N
    er_mieulerimpnwt = [er_mieulerimpnwt error_global_impnwt(@mitrapnwt,f,J,f_exact,intv,x0,n,TOL,nmax)];
end
er_mieulerimpfix=[];
for n=N
    er_mieulerimpfix = [er_mieulerimpfix error_global_imp(@mitrapfix,f,f_exact,intv,x0,n,TOL,nmax)];
end
loglog(N,er_mieulerimpnwt,"o-",'DisplayName', 'Trapecio Nwt')
hold on
loglog(N,er_mieulerimpfix,"d-",'DisplayName', 'Trapecio Fix')
grid on
title(s)
legend show
xlabel("N")
ylabel("Error")
hold off
%% Práctica 5 (Ecuación rígida con Euler implícito)
% Considerad el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haced un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la práctica anterior
%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfix* 
%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfixpc*
%% 
% * comparando los métodos de *mieulerimpnwtpc*, *mieulerimpnwtpc*

%% 
% * calcula la pendiente de las rectas
%% 
% *Solución*
disp('copy&paste cambiando datos')
%% Práctica 6 (Ecuación rígida con el trapecio)
% Repetid la práctica 5 pero con el método del trapecio de la práctica 2.
% 
%%
%Faltan cosas por que es mas de lo mismo pero cambiando los nombres de los
%metodos.
%% Apéndice: la implementación de las prácticas 1+2
function phaseplot(t,x,s)
sizeofx = size(x);
dimofx = sizeofx(1);
disp("Calling Pab's Plotting Function for")
disp(s)
if dimofx >= 3
    return
end
cc=['r-','r','r','g'];
nc=['t',"x_1(t)","x_2(t)","x_3(t)"];
figure('Name',"Componentes de la Soluci\'on",'NumberTitle','off');
if nargin == 3
end
for n = 1:dimofx
    subplot(dimofx,1,n)
    plot(t,x(n,:),cc(n))
    xlabel(nc(1))
    ylabel(nc(n+1))
end
figure('Name',"Trayectoria de la Soluci\'on",'NumberTitle','off')
if dimofx == 2
    plot(x(1,:),x(2,:),cc(end))
    title(s)
    xlabel(nc(2));
    ylabel(nc(3));
elseif dimofx == 3
    plot3(x(1,:),x(2,:),x(3,:),cc(end))
    xlabel(nc(2));
    ylabel(nc(3));
    zlabel(nc(4));
end
end
function [t,y,ev]=mieulerimpfix(f,intv,y0,N,TOL,nmax)
disp('H4: file: mieulerimpfix Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    ti = ti + h;
    g = @(x) y_ti + h*f(ti,x);
    [y_ti, ev_ti] = fixedPointIter(g,y_ti,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [x, counter] = fixedPointIter(f,x0,TOL,nmax)
%Fixed Point Iteration for finding roots of a function
% f = function whose roots are to be found
% x0 = starting point for the iteration 
% TOL = error tolerated
% nmax = maximum number of iterations before error message
x = x0;
counter = 0;
while ( max ( abs( f(x) - x ) ) >= TOL )
    counter = counter+1;
    x = f(x);
    if (counter == nmax)
        disp(x)
        disp(f(x))
        error('Check TOL, fixed point iteration does not converge')
    end
end
end
function [x, counter] = newtonRaphsonIter(f,jf,x0,TOL,nmax)
% Newton Raphson method for finding roots of a function
% f = function whose roots are to be found
% jh = jacobian matrix for the function 
% x0 = starting point for the iteration 
% TOL = error tolerated
% nmax = maximum number of iterations before error message
x = x0;
counter = 0;
while ( max( abs( f(x) ) )>= TOL )
    counter = counter+1;
    x = x - jf\f(x);
    if (counter == nmax)
        disp(f(x))
        error('Check TOL, fixed point iteration does not converge')
    end
end
end
function [t,y,ev]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax) 
disp('H4: file: mieulerimpfixpc Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    %prediccion por el metodo de euler
    pred = y_ti + h*f(ti,y_ti);
    ev = ev + 1;
    ti = ti + h;
    g = @(x) y_ti + h*f(ti,x);
    [y_ti, ev_ti] = fixedPointIter(g,pred,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [t,y,ev]=mieulerimpnwt(f,jfunc,intv,y0,N,TOL,nmax)
disp('H4: file: mieulerimpnwt Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    ti = ti + h;
    g = @(x) x - (y_ti + h*f(ti,x)) ;
    jg = eye(2) - h*jfunc;
    [y_ti, ev_ti] = newtonRaphsonIter(g,jg,y_ti,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [t,y,ev]=mieulerimpnwtpc(f,jfunc,intv,y0,N,TOL,nmax)
disp('H4: file: mieulerimpnwtpc Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    %prediccion por el metodo de euler
    pred = y_ti + h*f(ti,y_ti);
    ev = ev + 1;
    ti = ti + h;
    g = @(x) x - (y_ti + h*f(ti,x)) ;
    jg = eye(2) - h*jfunc;
    [y_ti, ev_ti] = newtonRaphsonIter(g,jg,pred,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [t,y,ev]=mitrapfix(f,intv,y0,N,TOL,nmax) 
disp('H4: file: mitrapfix Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    f_ti = f(ti,y_ti);
    ev = ev + 1;
    ti = ti + h;
    g = @(x) y_ti + h/2*(f_ti+f(ti,x));
    [y_ti, ev_ti] = fixedPointIter(g,y_ti,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [t,y,ev]=mitrapfixpc(f,intv,y0,N,TOL,nmax)
disp('H4: file: mitrapfixpc Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    f_ti = f(ti,y_ti);
    %prediccion por euler mej
    F1 = f(ti+h/2, y_ti+h/2*f_ti)
    y_pred = y_ti + h*F1;
    ev = ev + 2;
    ti = ti + h;
    g = @(x) y_ti + h/2*(f_ti+f(ti,x));
    [y_ti, ev_ti] = fixedPointIter(g,y_pred,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [t,y,ev]=mitrapnwt(f,jfunc,intv,y0,N,TOL,nmax)
disp('H4: file: mitrapnwt Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    f_ti = f(ti,y_ti);
    ev = ev + 1;
    ti = ti + h;
    g = @(x) x - (y_ti + h/2*(f_ti+f(ti,x)));
    jg = eye(2) - h/2*jfunc;
    [y_ti, ev_ti] = newtonRaphsonIter(g,jg,y_ti,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [t,y,ev]=mitrapnwtpc(f,jfunc,intv,y0,N,TOL,nmax)
disp('H4: file: mitrapnwtpc Pab')
ev = 0;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:N
    f_ti = f(ti,y_ti);
    %prediccion por euler mej
    F1 = f(ti+h/2, y_ti+h/2*f_ti)
    y_pred = y_ti + h*F1;
    ev = ev + 2;
    ti = ti + h;
    g = @(x) x - (y_ti + h/2*(f_ti+f(ti,x)));
    jg = eye(2) - h/2*jfunc;
    [y_ti, ev_ti] = newtonRaphsonIter(g,jg,y_pred,TOL,nmax);
    t = [t, ti];
    y = [y, y_ti];
    ev = ev + ev_ti;
end
end
function [err] = error_global_imp(met,f,f_exact,intv,x0,N,TOL,nmax)
% Global error related to the numerical integration given 
% met = @ handle for the numerical method
% f = ultivariate function for which we seek a numerical integration
% f_exact = theorethical value for the integration
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
y_exact = [];
[t,y_euler, eval] = met(f,intv,x0,N,TOL,nmax);
for i = t
    y_exact = [y_exact f_exact(i)];
end
er_M = abs(y_exact - y_euler);
err= max(er_M(:));
end
function [err] = error_global_impnwt(met,f,jf,f_exact,intv,x0,N,TOL,nmax)
% Global error related to the numerical integration given 
% met = @ handle for the numerical method
% f = ultivariate function for which we seek a numerical integration
% f_exact = theorethical value for the integration
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
y_exact = [];
[t,y_euler] = met(f,jf,intv,x0,N,TOL,nmax);
for i = t
    y_exact = [y_exact f_exact(i)];
end
er_M = abs(y_exact - y_euler);
err= max(er_M(:));
end
function [N_vect,Ev_vect,error_vect]=fcomparerrorimpnwt(met,func,jacfunc,intv,y0,N,yexact,M,TOL,nmax);
disp('H4: file: fcomparerrorimpnwt Alumno')
end 

function [N_vect,Ev_vect,error_vect]=fcomparerrorimpfix(met,func,intv,y0,N,yexact,M,TOL,nmax);
disp('H4: file: fcomparerrorimpfix Alumno')
end

function [p,q]=fcalcorden(N_vect,error_vect) 
disp('H4: file: fcalcorden Alumno')
end