%% Prácticas de Matlab
%% Diagrama de eficiencia con métodos monopaso explícitos
%% Hoja 3
% *Nombre: Pablo Cayo*
% 
% *Apellido: Alcalde Montes de Oca*
% 
%% 
% 
%% 1. Diagrama de eficiencia
% Práctica 1 (El método de Euler explícito) 
% Consideramos el siguiente problema lineal
% 
% $$   y^{\prime}(t)=Ay(t)+B(t) \quad\mbox{para} \quad 0\leq t\leq 10,\quad  
% y(0)=(2,3)^{T},$$
% 
% $$    A=\left(\begin{array}{cc}        -2 & 1\\        1 & -2      \end{array}\right)    
% \qquad    B(t) =\left(\begin{array}{l}        2\sin(t)\\        2(\cos(t)-\sin(t)      
% \end{array}\right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}\left(\begin{array}{l}      1\\      1    \end{array}\right)  
% +  \left(\begin{array}{l}      \sin(t)\\      \cos(t)    \end{array}\right)$$
% 
% Se pide lo siguiente
%% 
% # Resuelve este sistema mediante el método de _Euler explícito,_ almacena 
% el máximo en valor absoluto de la diferencia entre la solución exacta y la solución 
% numérica calculada.  *Indicación:* piensa qué norma vas a usar, dependiendo 
% del tipo de salida (vector columna o vector fila) que haya producido tu algoritmo. 
% Efectúa este cálculo para varias elecciones
% # del paso $h_j$ con $j=0,\ldots,7$ siendo $h_0=0.1$, $h_j=\frac{h_0}{2^j}$. 
% Almacena los diferentes valores de $h_i$ en un vector $h_{vect}$.
% # del número de puntos $N$ siendo $N_0=100$, $N_i=2^{i}N_0$. Almacena los 
% diferentes valores de $N_i$ en un vector $N_{vect}$.
% # número de las evaluaciones totales $Ev_i$ que realiza cada algoritmo para 
% cada valor de $h_i$. Almacena los valores en un vector $Ev_{vect}$.
% # Almacena los distintos errores en un vector de nombre  *error_euler*
%% 
% Además
%% 
% * Dibuja, en una misma ventana, en escala logarítmica, el error almacenado 
% en el apartado anterior frente al paso $h$,  $h_{vect}$ * Indicación:* usa el 
% comando |loglog| en vez del comando |plot|.
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $N_{vect}$ 
% * Calcula la pendiente da la recta.
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $Ev_{vect}$.
% * Interpreta el resultado.
%% Datos del Problema
disp('Eso es el codigo de Pab')
intv = [0 10];
x0 = [2; 3];
N0 = 100;
f = @(t,y) [-2*y(1)+ y(2) + 2*sin(t) ; y(1) - 2*y(2) + 2*(cos(t)-sin(t))];
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
s=sprintf("y'= [2y_1 + y_2 + 2sint; y_1 - 2y_2 +2(cost-sint)];\n intv=[%g %g];\n x0=[%g %g];",intv,x0);
N=[];

for j=1:7                               % UB:02.03.2020:
    N(j)=N0*2^j;
    h(j)=1/N(j);
end
%% Resolucion del Problema
Method_Handle={@mieuler, @mieulermod, @mieulermej, @mirk4};
Method_Name={'Euler','Euler Modificado', 'Euler Mejorado', 'Runge-Kutta 4'};
Method_N_Eval=[1,2,2,4];
% Me dispongo a crear una matrix 4xlength(N) en la que cada fila i
% contenga el error obtenido al usar N(j) para calcular el error usando el
% metodo i-esimo i.e.
%          ( error_global( mieuler, N(1) ) ... error_global( euler, N(7) ) )
% er_met = (         .           .                        .
%          (         .                         .          .
%          (   error_global( rk4 , N(1) )  ...  error_global( rk4, N(7) )  )
% para ya con todos los errores calculados solo tener que preocuparme de la
% representacion grafica.
er_met=[];
% UB:02.03.2020: No me queda claro para que haces esto
for i=1:4
    er_current_met=[];
    for n=N
        er_current_met = [er_current_met error_global(Method_Handle{i},f,f_exact,intv,x0,n)];
    end
    er_met = [er_met; er_current_met];
end
%% Grafica del Error vs N
figure(1)
for i=1:4
    loglog(N,er_met(i,:),"o-",'DisplayName', Method_Name{i})
    hold on
end
grid on
title(s)
legend show
xlabel("N")
ylabel("Error")
hold off
%% Grafica del Error vs h
figure(2)
for i=1:4
    slope = (log(er_met(i,6))-log(er_met(i,1)))/(log(h(6))-log(h(1)));
    disp(sprintf('The slope for %s is %.2g (rounded to the closest decimal)',Method_Name{i}, slope))
    loglog(h,er_met(i,:),"o-",'DisplayName', Method_Name{i})
    hold on
end
grid on
legend show
xlabel("h")
ylabel("Error")
hold off
%% Grafica del Error vs N evaluaciones
figure(3)
for i=1:4
    loglog(N*Method_N_Eval(i),er_met(i,:),"o-",'DisplayName', Method_Name{i})
    hold on
end
grid on
legend show
xlabel("N evaluaciones")
ylabel("Error")
hold off
%% Apéndice código: funciones de Euler, Euler modificado, Euler mejorado y Runge-Kutta 4, para calcular y pintar el diagrama de eficiencia y el orden
function [err] = error_global(met,f,f_exact,intv,x0,N)
% Global error related to the numerical integration given 
% met = @ handle for the numerical method
% f = ultivariate function for which we seek a numerical integration
% f_exact = theorethical value for the integration
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
y_exact = [];
[t,y_euler] = met(f,intv,x0,N);
for i = t
    y_exact = [y_exact f_exact(i)];
end
er_M = abs(y_exact - y_euler);
err= max(er_M(:));
end
function [t,y] = mieuler(f,intv,y0,N)
% Numerical integration using the Explicit Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    slope = f(ti,y_ti);
    ti = ti + h;
    y_ti = y_ti + h*slope;
    t = [t, ti];
    y = [y, y_ti];
end
end
function [t,y] = mieulermej(f,intv,y0,N)
% Numerical integration using the Improved Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    slope = f(ti,y_ti);
    slope2 = f(ti+h, y_ti+h*slope);
    ti = ti + h;
    y_ti = y_ti + h*(slope + slope2)/2;
    t = [t, ti];
    y = [y, y_ti];
end
end
function [t,y] = mieulermod(f,intv,y0,N)
% Numerical integration using the Modified Euler Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    slope = f(ti,y_ti);
    slope = f(ti+h/2, y_ti+h/2*slope);
    ti = ti + h;
    y_ti = y_ti + h*slope;
    t = [t, ti];
    y = [y, y_ti];
end
end
function [t,y] = mirk4(f,intv,y0,N)
% Numerical integration using the Runge-Kutta Order 4 Method for ODE's
% f = multivariate function to whose numerical integration we seek
% intv = [t0 tEnd]; t0 = initial time ; tEnd = final time of integration
% y0 = initial conditions for the IVP;
% N = number of subintervals for the integration;
t0 = intv(1);
tFin = intv(2);
ti = t0;
t = ti;
y_ti = y0;
y = y_ti;
h = (tFin-t0)/N;
for i = 1:1:N
    F1 = f(ti,y_ti);
    F2 = f(ti+h/2, y_ti+h/2*F1);
    F3 = f(ti+h/2, y_ti+h/2*F2);
    F4 = f(ti+h, y_ti+h*F3);
    phi = 1/6*(F1+F2*2+F3*2+F4);
    ti = ti + h;
    y_ti = y_ti + h*phi;
    t = [t, ti];
    y = [y, y_ti];
end
end